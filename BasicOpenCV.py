import numpy as np
import cv2
import pytesseract
import sys
from PIL import Image

pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files (x86)\Tesseract-OCR\tesseract.exe"
#def readImage():
   
def main():
    cap = cv2.VideoCapture(0)
    while(True):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imwrite("frame.jpg", frame)
        cv2.imshow('frame',gray)
        text = pytesseract.image_to_string(Image.open("frame.jpg"))
        print(text)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()
main()

